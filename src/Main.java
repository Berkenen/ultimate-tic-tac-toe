import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static void main(String args[]) {
        Random random = new Random();
        Scanner in = new Scanner(System.in);

        // game loop
        while (true) {
            int opponentRow = in.nextInt();
            int opponentCol = in.nextInt();
            int validActionCount = in.nextInt();
            List<String> validActions = new ArrayList<>(validActionCount);
            for (int i = 0; i < validActionCount; i++) {
                int row = in.nextInt();
                int col = in.nextInt();


                validActions.add(String.format("%s %s", row, col));
            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");





            String move = validActions.get(random.nextInt(validActions.size()));

            System.out.println(move);
        }
    }


    public static int getWinner(int[][] board){
        for(int i=0; i<3; i++){
            if(board[0][i] != 0
                    && board[0][i]==board[1][i]
                    && board[0][i]==board[2][i])
            {
                return board[0][i];
            }
            if(board[i][0] != 0
                    && board[i][0]==board[i][1]
                    && board[i][0]==board[i][2])
            {
                return board[i][0];
            }
            if (board[0][0] != 0 && board[0][0] == board[1][1] && board[0][0] == board[2][2])
            {
                return board[0][0];
            }
            if (board[2][0] != 0 && board[2][0] == board[1][1] && board[0][0] == board[0][2])
            {
                return board[0][0];
            }
        }
        return 0;
    }
}