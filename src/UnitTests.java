import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class UnitTests {

        void assertWinner(int expectedWinner, int[][] board){
            int actual = Player.getWinner(board);

            assertEquals(expectedWinner, actual);
        }



        @Test
        void rightColumnWinnerPlayer2() {
            assertWinner( 2, new int [][]{
                    {1,1,2},
                    {0,2,2},
                    {2,1,2}});

        }

        @Test
        void topRowWinner() {
            assertWinner( 1, new int [][]{
                {1,1,1},
                {0,2,2},
                {2,1,2}});

        }

        @Test
        void emptyBoard() {
            assertWinner( 0, new int [][]{
                    {0,0,0},
                    {0,0,0},
                    {0,0,0}});

        }

        @Test
        void middleColumnBoard() {
            assertWinner( 1, new int [][]{
                    {2,1,1},
                    {0,1,2},
                    {2,1,2}});

        }

        @Test
        void rightDiagonalWinner() {
            assertWinner(1, new int[][]{
                    {1, 2, 1},
                    {0, 1, 2},
                    {1, 1, 2}});

        }

    }

